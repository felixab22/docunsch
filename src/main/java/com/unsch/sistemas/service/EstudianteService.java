package com.unsch.sistemas.service;

import java.util.List;

import com.unsch.sistemas.model.documento.Estudiante;

public interface EstudianteService {

	Estudiante SaveEstudiante(Estudiante estudiante);

	List<Estudiante> GetListaEstudiante();

	Estudiante getEstudianteByCodigo(String codigo);
}
