package com.unsch.sistemas.service.impl;

import com.unsch.sistemas.dao.documento.IRoleDao;
import com.unsch.sistemas.model.documento.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unsch.sistemas.dao.documento.PersonalDAO;
import com.unsch.sistemas.model.documento.Personal;
import com.unsch.sistemas.service.PersonalService;
import java.util.List;

@Service
public class PersonalServiceImpl implements PersonalService {

    @Autowired
    protected PersonalDAO daoPersonal;

    @Autowired
    private IRoleDao roleDao;

    @Override
    public List<Personal> GetListaPersonal() {

        return this.daoPersonal.findAll();
    }

    @Override
    public List<Role> getAllRole() {
        return this.roleDao.findAll();
    }

    @Override
    public Personal SavePersonal(Personal personal) {

        return this.daoPersonal.save(personal);
    }
}
