package com.unsch.sistemas.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.unsch.sistemas.dto.SolicitudTipoDto;
import com.unsch.sistemas.model.documento.Escuela;
import com.unsch.sistemas.model.documento.Estado;
import com.unsch.sistemas.model.documento.Estadodocumento;
import com.unsch.sistemas.service.EstadodocumentoService;
import com.unsch.sistemas.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/estadodoc")
public class EstadodocumentoController {

    @Autowired
    private EstadodocumentoService estadodocumentoSrv;
    // convertir JSON en entitities y al reves
    @Autowired
    private ObjectMapper objEstadodocumento;

    @RequestMapping(value = "/SaveOrUpdateEstadodocumento", method = RequestMethod.POST)
    public RestResponse  GuardarEstadodocumento(@RequestBody String jsonEstadodocumento)
            throws  IOException {
        this.objEstadodocumento = new ObjectMapper();

        Estadodocumento estadodocumento = this.objEstadodocumento.readValue(jsonEstadodocumento, Estadodocumento.class);

        // hacer validaciones

        // decimos para guardar
        this.estadodocumentoSrv.SaveEstadodocumento(estadodocumento);
        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa", estadodocumento);
    }

    @GetMapping("/listaEstadodocumentos")
    public RestResponse listaEstadodocumento(@RequestParam Estado idestado, @RequestParam Escuela idescuela) {

        List<SolicitudTipoDto> result = this.estadodocumentoSrv.GetListaEstadodocumento(idestado, idescuela);

        if (result.isEmpty()){
            return new RestResponse(HttpStatus.NO_CONTENT.value(),"Registro vacio");
        }
        return new RestResponse(HttpStatus.OK.value(),"Registros ubicados", result);
    }
}
