package com.unsch.sistemas.service;

import java.util.List;

import com.unsch.sistemas.dto.SolicitudEstadosDto;
import com.unsch.sistemas.dto.SolicitudTipoDto;
import com.unsch.sistemas.model.documento.Estadodocumento;
import com.unsch.sistemas.model.documento.Solicitud;
import com.unsch.sistemas.model.documento.Tipodocumento;

public interface SolicitudService {

    Solicitud SaveSolicitud(Solicitud solicitud);

    List<Solicitud> GetListaSolicitud();

    /**
     * Obtiene Lista de las Solicitudes por el TIPO DE DOCUMENTO
     * @param tipodocumento
     * @return
     */
    List<SolicitudTipoDto> getSolicitudByTipoDoc(Tipodocumento tipodocumento);

    /**
     * Obtiene una SOLICITUD  atravez de su CODIGO
     * @param codigo
     * @return
     */
    SolicitudEstadosDto getSolicitudByCodigo(String codigo);
}
