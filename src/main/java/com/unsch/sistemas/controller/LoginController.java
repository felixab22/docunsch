package com.unsch.sistemas.controller;

import com.unsch.sistemas.dao.documento.IRoleDao;
import com.unsch.sistemas.dao.documento.IUserDao;
import com.unsch.sistemas.model.documento.Role;
import com.unsch.sistemas.model.documento.User;
import com.unsch.sistemas.payload.JwtAuthenticationResponse;
import com.unsch.sistemas.payload.LoginRequest;
import com.unsch.sistemas.payload.SignUpRequest;
import com.unsch.sistemas.security.JwtTokenProvider;
import com.unsch.sistemas.service.IPersonaService;
import com.unsch.sistemas.service.IUserService;
import com.unsch.sistemas.util.RestResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by AITAMH on 3/06/2020.
 */
@RestController
@RequestMapping("/auth")
public class LoginController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private IUserDao usuarioDao;

    @Autowired
    private IRoleDao roleDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtTokenProvider tokenProvider;

    @Autowired
    private IPersonaService personaService;

    @Autowired
    private IUserService userService;

    private static final Log LOGGER = LogFactory.getLog(LoginController.class);

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsernameOrEmail(),
                        loginRequest.getPassword()
                )
        );
//        LOGGER.info(" PARAMETROS DE LOGUEO: <<<<<<<>>>>>> '"+ loginRequest.getUsernameOrEmail() + "'");
//        LOGGER.info(" PARAMETROS DE LOGUEO: <<<<<<<>>>>>> '"+ loginRequest.getPassword() + "'");

        User user = this.userService.getPersonaByUsuario(loginRequest.getUsernameOrEmail());

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);

        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt, userDetails.getUsername(), userDetails.getAuthorities(), user.getEmpleado()));
    }

    @PostMapping("/signup")
    public RestResponse registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {

        LOGGER.info(" PARAMETROS DE LOGUEO: <<<<<<<>>>>>> '"+ signUpRequest.toString() + "'");

        if (usuarioDao.existsByUsername(signUpRequest.getUsername())) {
            return new RestResponse(HttpStatus.CONFLICT.value(),"Fail -> Username is already taken!");
//            return new ResponseEntity<>(new ResponseMessage(HttpStatus.BAD_REQUEST.value(),"Fail -> Username is already taken!"));
        }

        // Creating user's account
        User user = new User( signUpRequest.getUsername(),passwordEncoder.encode(signUpRequest.getPassword()),signUpRequest.getPersonal());

        Long strRoles = signUpRequest.getIdrole();

        Set<Role> roles = new HashSet<>();

        Role adminRole = this.roleDao.findByIdrol(strRoles);

        roles.add(adminRole);

        user.setRoles(roles);
        usuarioDao.save(user);

//        return new ResponseEntity<>(new ResponseMessage(HttpStatus.OK.value(), "User registered ");
        return new RestResponse(HttpStatus.OK.value(),"User registered sucefull", user);
    }
}


