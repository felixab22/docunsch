package com.unsch.sistemas.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unsch.sistemas.dao.documento.ConstanciaDAO;
import com.unsch.sistemas.model.documento.Constancia;
import com.unsch.sistemas.service.ConstanciaService;
import java.util.List;

@Service
public class ConstanciaServiceImpl implements ConstanciaService {

    @Autowired
    protected ConstanciaDAO daoConstancia;

    @Override
    public List<Constancia> GetListaConstancia() {

        return this.daoConstancia.findAll();
    }

    @Override
    public Constancia SaveConstancia(Constancia constancia) {

        return this.daoConstancia.save(constancia);
    }
}
