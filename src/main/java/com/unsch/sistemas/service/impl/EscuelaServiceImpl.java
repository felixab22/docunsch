package com.unsch.sistemas.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unsch.sistemas.dao.documento.EscuelaDAO;
import com.unsch.sistemas.model.documento.Escuela;
import com.unsch.sistemas.service.EscuelaService;
import java.util.List;

@Service
public class EscuelaServiceImpl implements EscuelaService {

    @Autowired
    protected EscuelaDAO daoEscuela;

    @Override
    public List<Escuela> GetListaEscuela() {

        return this.daoEscuela.findAll();
    }

    @Override
    public Escuela SaveEscuela(Escuela escuela) {

        return this.daoEscuela.save(escuela);
    }
}
