package com.unsch.sistemas.service;

import java.util.List;

import com.unsch.sistemas.model.documento.Personal;
import com.unsch.sistemas.model.documento.Role;

public interface PersonalService {

    Personal SavePersonal(Personal personal);

    List<Personal> GetListaPersonal();

    List<Role> getAllRole();
}
