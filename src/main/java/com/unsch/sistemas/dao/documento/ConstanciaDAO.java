package com.unsch.sistemas.dao.documento;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unsch.sistemas.model.documento.Constancia;

public interface ConstanciaDAO extends JpaRepository<Constancia, Long> {

}
