package com.unsch.sistemas.dao.documento;

import java.util.List;

import com.unsch.sistemas.model.documento.Solicitud;
import org.springframework.data.jpa.repository.JpaRepository;

import com.unsch.sistemas.model.documento.Carta;

public interface CartaDAO  extends JpaRepository<Carta, Long> {
	List<Carta> findByIdcarta(Long idcarta);
	Carta findByIdsolicitud(Solicitud idsolicitud);
}
