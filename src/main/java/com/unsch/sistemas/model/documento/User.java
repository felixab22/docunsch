package com.unsch.sistemas.model.documento;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by AITAMH on 3/06/2020.
 */
@Entity
@Table(name = "user", uniqueConstraints = {@UniqueConstraint(columnNames = { "username" })})
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Size(max = 15)
    private String username;

    @Size(max = 100)
    private String password;

    @OneToOne
    @JoinColumn(name = "idpersona", referencedColumnName = "idpersona")
    private Personal personal;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    public User() {
    }

    public User(Long id) {
        this.id = id;
    }

    public User(String username, String password, Personal personal) {
        this.username = username;
        this.password = password;
        this.personal = personal;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Personal getEmpleado() {
        return personal;
    }

    public void setEmpleado(Personal empleado) {
        this.personal = empleado;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", persona=" + personal +
                ", roles=" + roles +
                '}';
    }
}
