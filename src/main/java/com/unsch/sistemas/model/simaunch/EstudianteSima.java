package com.unsch.sistemas.model.simaunch;

import javax.persistence.*;

@Entity
@Table(name = "estudiante")
public class EstudianteSima {

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "idestudiante")
    private Long idestudiante;

    @Column(name = "codigo")
    private String codigo;

    @Column(name = "dni")
    private String dni;

    @Column(name = "apellido")
    private String apellido;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "direccion")
    private String direccion;

    @Column(name = "sexo")
    private String sexo;

    @Column(name = "idescuela")
    private int idescuela;

    @Column(name = "escuela")
    private String escuela;

    @Column(name = "idfacultad")
    private int idfacultad;

    @Column(name = "facultad")
    private String facultad;

    @Column(name = "semestre_primera_matricula")
    private String semestre_primera_matricula;

    @Column(name = "semestre_ultima_matricula")
    private String semestre_ultima_matricula;

    public EstudianteSima() {
    }

    public EstudianteSima(String codigo, String dni, String apellido, String nombre, String direccion, String sexo, int idescuela, String escuela, int idfacultad, String facultad, String semestre_primera_matricula, String semestre_ultima_matricula) {
        this.codigo = codigo;
        this.dni = dni;
        this.apellido = apellido;
        this.nombre = nombre;
        this.direccion = direccion;
        this.sexo = sexo;
        this.idescuela = idescuela;
        this.escuela = escuela;
        this.idfacultad = idfacultad;
        this.facultad = facultad;
        this.semestre_primera_matricula = semestre_primera_matricula;
        this.semestre_ultima_matricula = semestre_ultima_matricula;
    }

    public Long getIdestudiante() {
        return idestudiante;
    }

    public void setIdestudiante(Long idestudiante) {
        this.idestudiante = idestudiante;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellidos) {
        this.apellido = apellidos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public int getIdescuela() {
        return idescuela;
    }

    public void setIdescuela(int idescuela) {
        this.idescuela = idescuela;
    }

    public String getEscuela() {
        return escuela;
    }

    public void setEscuela(String escuela) {
        this.escuela = escuela;
    }

    public int getIdfacultad() {
        return idfacultad;
    }

    public void setIdfacultad(int idfacultad) {
        this.idfacultad = idfacultad;
    }

    public String getFacultad() {
        return facultad;
    }

    public void setFacultad(String facultad) {
        this.facultad = facultad;
    }

    public String getSemestre_primera_matricula() {
        return semestre_primera_matricula;
    }

    public void setSemestre_primera_matricula(String semestre_primera_matricula) {
        this.semestre_primera_matricula = semestre_primera_matricula;
    }

    public String getSemestre_ultima_matricula() {
        return semestre_ultima_matricula;
    }

    public void setSemestre_ultima_matricula(String semestre_ultima_matricula) {
        this.semestre_ultima_matricula = semestre_ultima_matricula;
    }
}
