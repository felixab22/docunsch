package com.unsch.sistemas.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.unsch.sistemas.model.documento.Grados;
import com.unsch.sistemas.service.GradosService;
import com.unsch.sistemas.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/grado")
public class GradosController {

    @Autowired
    protected GradosService alumnoSrv;
    // convertir JSON en entitities y al reves
    @Autowired
    protected ObjectMapper objGrados;

    @RequestMapping(value = "/SaveOrUpdateGrados", method = RequestMethod.POST)
    public RestResponse GuardarGrados(@RequestBody String jsonGrados)
            throws JsonParseException, JsonMappingException, IOException {
        this.objGrados = new ObjectMapper();

        Grados grados = this.objGrados.readValue(jsonGrados, Grados.class);

        // hacer validaciones

        // decimos para guardar
        this.alumnoSrv.SaveGrados(grados);
        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa", grados);
    }

    @GetMapping("/listaGradoss")
    public RestResponse GetListaGrados() {
    List<Grados> result = this.alumnoSrv.GetListaGrados();
    if (result.isEmpty()){
        return new RestResponse(HttpStatus.NO_CONTENT.value(),"No se encuentra registrado ningun tipo de documento");
    }
    return new RestResponse(HttpStatus.OK.value(),"Registros ubicados", result);
    }
}
