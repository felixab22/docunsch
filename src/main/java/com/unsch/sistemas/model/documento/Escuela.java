package com.unsch.sistemas.model.documento;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "escuela")
@Access(AccessType.FIELD)

public class Escuela implements Serializable{
	
	
	private static final long serialVersionUID = 1L;
	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    @Basic(optional = false)
	    @Column(name = "idescuela")
	    private Long idescuela;
	    
	    @Column(name = "nombre")
	    private String nombre;

	    @JoinColumn(name = "idfacultad", referencedColumnName = "idfacultad")
	    @ManyToOne
	    private Facultad idfacultad;

	    public Escuela() {			
		}

	public Escuela(Facultad idfacultad) {
		this.idfacultad = idfacultad;
	}

	public Escuela(Long idescuela) {
			this.idescuela = idescuela;
		}

		public Escuela(Long idescuela, String nombre, Facultad idfacultad) {
			this.idescuela = idescuela;
			this.nombre = nombre;
			this.idfacultad = idfacultad;
		}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Long getIdescuela() {
		return idescuela;
	}

	public void setIdescuela(Long idescuela) {
		this.idescuela = idescuela;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Facultad getIdfacultad() {
		return idfacultad;
	}

	public void setIdfacultad(Facultad idfacultad) {
		this.idfacultad = idfacultad;
	}

	@Override
		    public String toString() {
		        return "Escuela{" +
		                "idescuela=" + idescuela +
		                ", nombre='" + nombre +
		                ", idfacultad='" + idfacultad + 
		                '}';
		    }


		
}
