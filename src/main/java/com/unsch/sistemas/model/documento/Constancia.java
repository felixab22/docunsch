package com.unsch.sistemas.model.documento;

import javax.persistence.*;
import java.util.Date;
import java.io.Serializable;

@Entity
@Table(name = "constancia")
@Access(AccessType.FIELD)
public class Constancia implements Serializable {
	
	private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idconstancia")
    private Long idconstancia;
    
    @Column(name = "tipoconstancia")
    private String tipoconstancia;
         
    @Column(name = "fechaemision")
	@Temporal(TemporalType.DATE)
    private Date fechaemision;

    @JoinColumn(name = "idsolicitud", referencedColumnName = "idsolicitud")
    @ManyToOne
    private Solicitud idsolicitud;

	public Constancia() {		
	}

    public Constancia(String tipoconstancia, Date fechaemision, Solicitud idsolicitud) {
        this.tipoconstancia = tipoconstancia;
        this.fechaemision = fechaemision;
        this.idsolicitud = idsolicitud;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getIdconstancia() {
        return idconstancia;
    }

    public void setIdconstancia(Long idconstancia) {
        this.idconstancia = idconstancia;
    }

    public String getTipoconstancia() {
        return tipoconstancia;
    }

    public void setTipoconstancia(String tipoconstancia) {
        this.tipoconstancia = tipoconstancia;
    }

    public Date getFechaemision() {
        return fechaemision;
    }

    public void setFechaemision(Date fechaemision) {
        this.fechaemision = fechaemision;
    }

    public Solicitud getIdsolicitud() {
        return idsolicitud;
    }

    public void setIdsolicitud(Solicitud idsolicitud) {
        this.idsolicitud = idsolicitud;
    }

    @Override
    public String toString() {
        return "constancia{" +
                "idconstancia=" + idconstancia +
                ", tipoconstancia='" + tipoconstancia +
                ", fechaemision='" + fechaemision +
                ", idsolicitud='" + idsolicitud + 
                '}';
    } 
    
    
}
