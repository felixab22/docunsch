package com.unsch.sistemas.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unsch.sistemas.dao.documento.PracticasDAO;
import com.unsch.sistemas.model.documento.Practicas;
import com.unsch.sistemas.service.PracticasService;
import java.util.List;

@Service
public class PracticasServiceImpl implements PracticasService {

    @Autowired
    protected PracticasDAO daoPracticas;

    @Override
    public List<Practicas> GetListaPracticas() {

        return this.daoPracticas.findAll();
    }

    @Override
    public Practicas SavePracticas(Practicas practicas) {

        return this.daoPracticas.save(practicas);
    }
}
