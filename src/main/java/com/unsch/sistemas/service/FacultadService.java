package com.unsch.sistemas.service;

import java.util.List;

import com.unsch.sistemas.model.documento.Facultad;

public interface FacultadService {

    Facultad SaveFacultad(Facultad facultad);

    List<Facultad> GetListaFacultad();
}
