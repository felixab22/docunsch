package com.unsch.sistemas.model.documento;

import javax.persistence.*;
import java.util.Date;
import java.io.Serializable;

@Entity
@Table(name = "grados")
@Access(AccessType.FIELD)
public class Grados implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idgrados")
    private Long idgrados;

    @Column(name = "fechaegreso")
    @Temporal(TemporalType.DATE)
    private Date fechaegreso;

    @Column(name = "fechabachiller")
    @Temporal(TemporalType.DATE)
    private Date fechabachiller;

    @Column(name = "resolbachiller")
    private String resolbachiller;

    @Column(name = "fechatitulo")
    @Temporal(TemporalType.DATE)
    private Date fechatitulo;

    @Column(name = "resoltitulo")
    private String resoltitulo;

    @JoinColumn(name = "idpersona", referencedColumnName = "idpersona")
    @ManyToOne
    private Estudiante idestudiante;

    public Grados() {
    }

    public Grados(Long idgrados) {
        this.idgrados = idgrados;
    }

    public Grados(Long idgrados, Date fechaegreso, Date fechabachiller, String resolbachiller, Date fechatitulo,
            String resoltitulo, Estudiante idestudiante) {
        this.idgrados = idgrados;
        this.fechaegreso = fechaegreso;
        this.fechabachiller = fechabachiller;
        this.resolbachiller = resolbachiller;
        this.fechatitulo = fechatitulo;
        this.resoltitulo = resoltitulo;
        this.idestudiante = idestudiante;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getIdgrados() {
        return idgrados;
    }

    public void setIdgrados(Long idgrados) {
        this.idgrados = idgrados;
    }

    public Date getFechaegreso() {
        return fechaegreso;
    }

    public void setFechaegreso(Date fechaegreso) {
        this.fechaegreso = fechaegreso;
    }

    public Date getFechabachiller() {
        return fechabachiller;
    }

    public void setFechabachiller(Date fechabachiller) {
        this.fechabachiller = fechabachiller;
    }

    public String getResolbachiller() {
        return resolbachiller;
    }

    public void setResolbachiller(String resolbachiller) {
        this.resolbachiller = resolbachiller;
    }

    public Date getFechatitulo() {
        return fechatitulo;
    }

    public void setFechatitulo(Date fechatitulo) {
        this.fechatitulo = fechatitulo;
    }

    public String getResoltitulo() {
        return resoltitulo;
    }

    public void setResoltitulo(String resoltitulo) {
        this.resoltitulo = resoltitulo;
    }

    public Estudiante getIdestudiante() {
        return idestudiante;
    }

    public void setIdestudiante(Estudiante idestudiante) {
        this.idestudiante = idestudiante;
    }

    @Override
    public String toString() {
        return "grados{" + "idgrados=" + idgrados + ", fechaegreso='" + fechaegreso + ", fechabachiller='"
                + fechabachiller + ", resolbachiller='" + resolbachiller + ", fechatitulo='" + fechatitulo
                + ", resoltitulo='" + resoltitulo + ", idestudiante='" + idestudiante + '}';
    }

}
