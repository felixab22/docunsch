package com.unsch.sistemas.model.documento;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "solicitud")
//@Access(AccessType.FIELD)
public class Solicitud implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idsolicitud")
    private Long idsolicitud;

    @Column(name = "codigo")
    private String codigo;

    @Column(name = "fechasolicitud")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date fechasolicitud;

    @NotNull
    @Column(name = "boletaelectronica")
    private String boletaelectronica;

    @OneToOne
    @JoinColumn(name = "idpersona")
    private Estudiante idestudiante;

    @ManyToOne
    @JoinColumn(name = "tipodocumento")
    private Tipodocumento tipodocumento;

//    @JsonIgnore
    @OneToMany(mappedBy = "idsolicitud", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Estadodocumento> estados = new ArrayList<>();

    public Solicitud() {
    }

    public Solicitud(Long idsolicitud) {
        this.idsolicitud = idsolicitud;
    }

    public Solicitud(String codigo, Date fechasolicitud, String boletaelectronica, Estudiante idestudiante, Tipodocumento tipodocumento) {
        this.codigo = codigo;
        this.fechasolicitud = fechasolicitud;
        this.boletaelectronica = boletaelectronica;
        this.idestudiante = idestudiante;
        this.tipodocumento = tipodocumento;
    }

    public Long getIdsolicitud() {
        return idsolicitud;
    }

    public void setIdsolicitud(Long idsolicitud) {
        this.idsolicitud = idsolicitud;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Date getFechasolicitud() {
        return fechasolicitud;
    }

    public void setFechasolicitud(Date fechasolicitud) {
        this.fechasolicitud = fechasolicitud;
    }

    public String getBoletaelectronica() {
        return boletaelectronica;
    }

    public void setBoletaelectronica(String boletaelectronica) {
        this.boletaelectronica = boletaelectronica;
    }

    public Estudiante getIdestudiante() {
        return idestudiante;
    }

    public void setIdestudiante(Estudiante idestudiante) {
        this.idestudiante = idestudiante;
    }

    public Tipodocumento getTipodocumento() {
        return tipodocumento;
    }

    public void setTipodocumento(Tipodocumento tipodocumento) {
        this.tipodocumento = tipodocumento;
    }

    public List<Estadodocumento> getEstados() {
        return estados;
    }

    public void setEstados(List<Estadodocumento> estados) {
        this.estados = estados;
    }
}
