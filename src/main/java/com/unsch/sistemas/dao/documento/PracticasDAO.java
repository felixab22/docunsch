package com.unsch.sistemas.dao.documento;

import org.springframework.data.jpa.repository.JpaRepository;
import com.unsch.sistemas.model.documento.Practicas;
import java.util.List;

public interface PracticasDAO extends JpaRepository<Practicas, Long> {
    List<Practicas> findByIdpracticas(Long idpracticas);
}