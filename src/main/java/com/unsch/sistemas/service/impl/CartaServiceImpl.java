package com.unsch.sistemas.service.impl;

import java.util.List;

import com.unsch.sistemas.model.documento.Solicitud;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unsch.sistemas.dao.documento.CartaDAO;
import com.unsch.sistemas.model.documento.Carta;
import com.unsch.sistemas.service.CartaService;

@Service
public class CartaServiceImpl implements CartaService{

	@Autowired
	protected CartaDAO daoCarta;
	
	@Override
	public List<Carta> GetListaCarta() {
		
		return this.daoCarta.findAll();
	}

	@Override
	public Carta buscarCartaByIdsolicitud(Solicitud idsolicitud) {
		return daoCarta.findByIdsolicitud(idsolicitud);
	}

	@Override
	public Carta SaveCarta(Carta carta) {				
		
		return this.daoCarta.save(carta);
	}
}
