package com.unsch.sistemas.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.unsch.sistemas.model.documento.Escuela;
import com.unsch.sistemas.service.EscuelaService;
import com.unsch.sistemas.util.RestResponse;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/escuela")
public class EscuelaController {

    @Autowired
    protected EscuelaService alumnoSrv;
    // convertir JSON en entitities y al reves
    @Autowired
    protected ObjectMapper objEscuela;

    @RequestMapping(value = "/SaveOrUpdateEscuela", method = RequestMethod.POST)
    public RestResponse GuardarEscuela(@RequestBody String jsonEscuela)
            throws JsonParseException, JsonMappingException, IOException {
        this.objEscuela = new ObjectMapper();

        Escuela escuela = this.objEscuela.readValue(jsonEscuela, Escuela.class);

        // hacer validaciones

        // decimos para guardar
        this.alumnoSrv.SaveEscuela(escuela);
        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa", escuela);
    }

    @GetMapping("/listaEscuelas")
    public  RestResponse GetListaEscuela(){
    
    List<Escuela> result = this.alumnoSrv.GetListaEscuela();
    if (result.isEmpty()){
        return new RestResponse(HttpStatus.NO_CONTENT.value(),"No se encuentra registrado ningun tipo de documento");
    }
    return new RestResponse(HttpStatus.OK.value(),"Registros ubicados", result);
    }
}
