package com.unsch.sistemas.dao.documento;

import com.unsch.sistemas.model.documento.Persona;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPersonaDao extends JpaRepository<Persona,Long> {
}
