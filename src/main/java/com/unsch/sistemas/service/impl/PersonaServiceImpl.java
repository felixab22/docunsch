package com.unsch.sistemas.service.impl;

import com.unsch.sistemas.dao.documento.IPersonaDao;
import com.unsch.sistemas.model.documento.Persona;
import com.unsch.sistemas.service.IPersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class PersonaServiceImpl implements IPersonaService {

    @Autowired
    private IPersonaDao personaDao;

    @Override
    public Persona savePersona(Persona persona) {
        return this.personaDao.save(persona);
    }

    @Override
    public List<Persona> getAllPersona() {
        return this.personaDao.findAll();
    }
}
