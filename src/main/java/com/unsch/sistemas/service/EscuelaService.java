package com.unsch.sistemas.service;

import java.util.List;

import com.unsch.sistemas.model.documento.Escuela;

public interface EscuelaService {

    Escuela SaveEscuela(Escuela escuela);

    List<Escuela> GetListaEscuela();
}
