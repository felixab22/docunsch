package com.unsch.sistemas.dao.documento;

import org.springframework.data.jpa.repository.JpaRepository;
import com.unsch.sistemas.model.documento.Estudiante;
import java.util.List;

public interface EstudianteDAO extends JpaRepository<Estudiante, Long> {

	List<Estudiante> findByIdpersona(Long idestudiante);

	Estudiante findEstudianteByCodigo(String codigo);

}
