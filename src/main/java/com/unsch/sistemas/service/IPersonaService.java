package com.unsch.sistemas.service;


import com.unsch.sistemas.model.documento.Persona;

import java.util.List;

public interface IPersonaService {

    Persona savePersona(Persona persona);

    List<Persona> getAllPersona();
}
