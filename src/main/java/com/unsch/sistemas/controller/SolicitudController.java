package com.unsch.sistemas.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.unsch.sistemas.dao.documento.SolicitudDAO;
import com.unsch.sistemas.dto.SolicitudEstadosDto;
import com.unsch.sistemas.dto.SolicitudTipoDto;
import com.unsch.sistemas.model.documento.Solicitud;
import com.unsch.sistemas.model.documento.Tipodocumento;
import com.unsch.sistemas.service.EstadodocumentoService;
import com.unsch.sistemas.service.SolicitudService;
import com.unsch.sistemas.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/solicitud")
public class SolicitudController {

    @Autowired
    protected SolicitudService alumnoSrv;

    @Autowired
    private SolicitudDAO solicitudDAO;

    @Autowired
    private EstadodocumentoService estadodocumentoService;

    // convertir JSON en entitities y al reves
    @Autowired
    private ObjectMapper objSolicitud;

    @PostMapping("/SaveOrUpdateSolicitud")
    public RestResponse GuardarSolicitud(@RequestBody String jsonSolicitud) throws  IOException {
        this.objSolicitud = new ObjectMapper();

        Solicitud solicitud = this.objSolicitud.readValue(jsonSolicitud, Solicitud.class);

        // hacer validaciones

        // decimos para guardar
        this.alumnoSrv.SaveSolicitud(solicitud);
        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa", solicitud);
    }

    @GetMapping("/listaSolicitudes")
    public List<Solicitud> listaSolicitud() {
        return this.alumnoSrv.GetListaSolicitud();
    }

    @GetMapping("/listaSolicitudPorTipo")
    public RestResponse listaSolicitudPorTipo(@RequestParam Tipodocumento idtipodocumento) {

        List<SolicitudTipoDto> result = this.alumnoSrv.getSolicitudByTipoDoc(idtipodocumento);

        if (result.isEmpty()){
            return new RestResponse(HttpStatus.NO_CONTENT.value(),"Registro vacio");
        }
        return new RestResponse(HttpStatus.OK.value(),"Registros ubicados", result);

    }

    @GetMapping("/getSolicitudByCodigo")
    public RestResponse getSolicitudByCodigo(@RequestParam String codigodoc) {

        SolicitudEstadosDto result = this.alumnoSrv.getSolicitudByCodigo(codigodoc);

        if (result == null){
            return new RestResponse(HttpStatus.NO_CONTENT.value(),"Registro vacio");
        }
        return new RestResponse(HttpStatus.OK.value(),"Registro ubicado", result);
    }

    @GetMapping("/reportSolicitudByAnio")
    public RestResponse reportSolicitudByAnio(@RequestParam String anio) {

        List<Integer> result = this.solicitudDAO.listaSolicitudPorTipoYanio(anio);

        if (result == null){
            return new RestResponse(HttpStatus.NO_CONTENT.value(),"Registro vacio");
        }
        return new RestResponse(HttpStatus.OK.value(),"Registro ubicado", result);
    }


    @GetMapping("/reportSolicitudByTipoDocumento")
    public RestResponse reportSolicitudByTipodocumento(@RequestParam Tipodocumento tipodocumento) {

        List<Integer> result = this.solicitudDAO.getSolicitudByAnio(tipodocumento);

        if (result == null){
            return new RestResponse(HttpStatus.NO_CONTENT.value(),"Registro vacio");
        }
        return new RestResponse(HttpStatus.OK.value(),"Registro ubicado", result);
    }
}
