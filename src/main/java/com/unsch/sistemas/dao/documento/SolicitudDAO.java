package com.unsch.sistemas.dao.documento;

import com.unsch.sistemas.model.documento.Tipodocumento;
import org.springframework.data.jpa.repository.JpaRepository;
import com.unsch.sistemas.model.documento.Solicitud;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SolicitudDAO extends JpaRepository<Solicitud, Long> {

    List<Solicitud> findByIdsolicitud(Long idsolicitud);

    Solicitud findTopByOrderByIdsolicitudDesc();

    List<Solicitud> findAllByTipodocumento(Tipodocumento idtipodocumento);

    Solicitud findByCodigo(String codigo);

    // Reportes
    @Query("SELECT COUNT(s), s.tipodocumento FROM Solicitud s WHERE SUBSTRING(s.fechasolicitud,1,4) = :anio GROUP BY s.tipodocumento")
    List<Integer> listaSolicitudPorTipoYanio(@Param("anio") String anio);

    @Query("select COUNT(s) FROM Solicitud s WHERE s.tipodocumento = :idtipodocumento GROUP BY SUBSTRING(s.fechasolicitud,1,4)")
    List<Integer> getSolicitudByAnio(@Param("idtipodocumento") Tipodocumento tipodocumento);

    // SELECT SUBSTRING(s.fechasolicitud,1,4) as "Año", s.tipodocumento ,COUNT(*) as "cantidad" FROM solicitud s
    // WHERE s.tipodocumento = 1
    // GROUP BY SUBSTRING(s.fechasolicitud,1,4)
}
