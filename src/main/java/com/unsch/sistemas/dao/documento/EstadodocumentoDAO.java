package com.unsch.sistemas.dao.documento;

import com.unsch.sistemas.model.documento.Escuela;
import com.unsch.sistemas.model.documento.Estado;
import com.unsch.sistemas.model.documento.Solicitud;
import org.springframework.data.jpa.repository.JpaRepository;
import com.unsch.sistemas.model.documento.Estadodocumento;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EstadodocumentoDAO extends JpaRepository<Estadodocumento, Long> {

    @Query("SELECT e FROM Estadodocumento e WHERE e.idestado = :idestado AND e.idsolicitud.idestudiante.idescuela = :idescuela")
    List<Estadodocumento> findAllByEstadoDocumento(@Param("idestado")Estado idestado, @Param("idescuela") Escuela idescuela);

    @Query("SELECT e FROM Estadodocumento e WHERE e.idsolicitud.codigo = :codigodocumento")
    List<Estadodocumento> findByCodigoDocumento(@Param("codigodocumento") String codigodocumento);

    Boolean existsByIdsolicitud(Solicitud idsolicitud);
}
