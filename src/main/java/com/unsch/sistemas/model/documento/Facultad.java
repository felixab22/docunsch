package com.unsch.sistemas.model.documento;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "facultad")

public class Facultad implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idfacultad")
    private Long idfacultad;

    @Size(max = 500)
    @Column(name = "nombre")
    private String nombre;

    public Facultad() {

    }

    public Facultad(Long idfacultad) {
        this.idfacultad = idfacultad;
    }

    public Facultad(Long idfacultad, String nombre, Long codigof) {
        this.idfacultad = idfacultad;
        this.nombre = nombre;

    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getIdfacultad() {
        return idfacultad;
    }

    public void setIdfacultad(Long idfacultad) {
        this.idfacultad = idfacultad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }



    @Override
    public String toString() {
        return "Facultad{" + "idfacultad=" + idfacultad + ", nombre='" + nombre +'}';
    }
}
