package com.unsch.sistemas.model.documento;

import javax.persistence.*;

@Entity
@Table(name = "tipodocumento")
public class Tipodocumento {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idtipodocumento")
    private Long idtipodocumento;

    @Column(name = "nombre")
    private String nombre;

    public Tipodocumento() {

    }
    public Tipodocumento(Long idtipodocumento) {
        this.idtipodocumento = idtipodocumento;
    }
    public Tipodocumento(Long idtipodocumento, String nombre) {
        this.idtipodocumento = idtipodocumento;
        this.nombre = nombre;
    }

    public Long getIdtipodocumento() {
        return idtipodocumento;
    }

    public void setIdtipodocumento(Long idtipodocumento) {
        this.idtipodocumento = idtipodocumento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Estadodocumento{" +
                "idtipodocumento=" + idtipodocumento +
                ", nombre='" + nombre +
                '}';
    }

}
