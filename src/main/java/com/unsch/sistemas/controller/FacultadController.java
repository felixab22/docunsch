package com.unsch.sistemas.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.unsch.sistemas.model.documento.Facultad;
import com.unsch.sistemas.service.FacultadService;
import com.unsch.sistemas.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/facultad")
public class FacultadController {

    @Autowired
    protected FacultadService alumnoSrv;
    // convertir JSON en entitities y al reves
    @Autowired
    protected ObjectMapper objFacultad;

    @RequestMapping(value = "/SaveOrUpdateFacultad", method = RequestMethod.POST)
    public RestResponse  GuardarFacultad(@RequestBody String jsonFacultad)
            throws JsonParseException, JsonMappingException, IOException {
        this.objFacultad = new ObjectMapper();

        Facultad facultad = this.objFacultad.readValue(jsonFacultad, Facultad.class);

        // hacer validaciones

        // decimos para guardar
        this.alumnoSrv.SaveFacultad(facultad);
        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa", facultad);
    }

    @GetMapping("/listaFacultads")
    public RestResponse GetListaFacultad() {

    List<Facultad> result = this.alumnoSrv.GetListaFacultad();
    if (result.isEmpty()){
        return new RestResponse(HttpStatus.NO_CONTENT.value(),"No se encuentra registrado ningun tipo de documento");
    }
    return new RestResponse(HttpStatus.OK.value(),"Registros ubicados", result);
    }
}
