package com.unsch.sistemas.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.unsch.sistemas.model.documento.Personal;
import com.unsch.sistemas.model.documento.Role;
import com.unsch.sistemas.service.PersonalService;
import com.unsch.sistemas.util.RestResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * Created by AITAMH on 5/06/2020.
 */
@RestController
@RequestMapping("/persona")
public class PersonaController {

        @Autowired
        private PersonalService personalService;


        @Autowired
        protected ObjectMapper objectMapper;

        private static final Log LOGGER = LogFactory.getLog(PersonaController.class);

        @PostMapping("/saveOrUpdateEmpleado")
        public RestResponse saveOrUpdateEmpleado(@RequestBody String empleadoJson)throws IOException {

        this.objectMapper = new ObjectMapper();
        LOGGER.info(" PARAMETROS DE ENTRADA: '"+ empleadoJson + "'");

        Personal empleado = this.objectMapper.readValue(empleadoJson, Personal.class);

        //if (validate(empleado)){
        //  return new RestResponse(HttpStatus.NOT_ACCEPTABLE.value(),"Los campos obligatorios estan vacios");
        //}
        this.personalService.SavePersonal(empleado);
        return new RestResponse(HttpStatus.OK.value(),"Correct registered", empleado);
        }

        @GetMapping("/getAllEmpleado")
        public RestResponse getAllEmpleado(){

        List<Personal> result = this.personalService.GetListaPersonal();

        if (result == null){
        return new RestResponse(HttpStatus.NO_CONTENT.value(),"Aun no se encuentran registros");
        }
        return new RestResponse(HttpStatus.OK.value(),"Lista de personas",result);
        }

        @GetMapping("/getAllRol")
        public RestResponse getAllRol(){

            List<Role> result = this.personalService.getAllRole();

            if (result == null){
                return new RestResponse(HttpStatus.NO_CONTENT.value(),"Registro vacio");
            }
            return new RestResponse(HttpStatus.OK.value(),"Registros ubicados",result);
        }

}
