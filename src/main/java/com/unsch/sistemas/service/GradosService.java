package com.unsch.sistemas.service;

import java.util.List;

import com.unsch.sistemas.model.documento.Grados;

public interface GradosService {

    Grados SaveGrados(Grados grados);

    List<Grados> GetListaGrados();
}
