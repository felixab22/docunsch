package com.unsch.sistemas.model.documento;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@PrimaryKeyJoinColumn(referencedColumnName="idpersona")
public class Estudiante extends Persona {

	@Column(name = "codigo")
	private String codigo;

	@Column(name = "direccion")
	private String direccion;

	@Column(name = "sexo")
	private String sexo;

	@Column(name = "semestreinicio")
	private String semestreinicio;

	@Column(name = "semestrefin")
	private String semestrefin;

	@Column(name = "fechainicio")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fechainicio;

	@Column(name = "fechafin")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fechafin;

	@Column(name = "fechaserie")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fechaserie;

	@Column(name = "serie")
	private String serie;

	@Column(name = "creditos")
	private Long creditos;

	@ManyToOne
	@JoinColumn(name = "idescuela", referencedColumnName = "idescuela")
	private Escuela idescuela;

	public Estudiante() {

	}

	public Estudiante(Long idpersona) {
		super(idpersona);
	}

	public Estudiante(String codigo, String direccion, String sexo, String semestreinicio, String semestrefin, Date fechainicio, Date fechafin, Date fechaserie, String serie, Long creditos, Escuela idescuela) {
		this.codigo = codigo;
		this.direccion = direccion;
		this.sexo = sexo;
		this.semestreinicio = semestreinicio;
		this.semestrefin = semestrefin;
		this.fechainicio = fechainicio;
		this.fechafin = fechafin;
		this.fechaserie = fechaserie;
		this.serie = serie;
		this.creditos = creditos;
		this.idescuela = idescuela;
	}

	public Estudiante(Long idpersona, String codigo, String direccion, String sexo, String semestreinicio, String semestrefin, Date fechainicio, Date fechafin, Date fechaserie, String serie, Long creditos, Escuela idescuela) {
		super(idpersona);
		this.codigo = codigo;
		this.direccion = direccion;
		this.sexo = sexo;
		this.semestreinicio = semestreinicio;
		this.semestrefin = semestrefin;
		this.fechainicio = fechainicio;
		this.fechafin = fechafin;
		this.fechaserie = fechaserie;
		this.serie = serie;
		this.creditos = creditos;
		this.idescuela = idescuela;
	}

	public Estudiante(String nombre, String apellido, String dni, String codigo, String direccion, String sexo, String semestreinicio, String semestrefin, Date fechainicio, Date fechafin, Date fechaserie, String serie, Long creditos, Escuela idescuela) {
		super(nombre, apellido, dni);
		this.codigo = codigo;
		this.direccion = direccion;
		this.sexo = sexo;
		this.semestreinicio = semestreinicio;
		this.semestrefin = semestrefin;
		this.fechainicio = fechainicio;
		this.fechafin = fechafin;
		this.fechaserie = fechaserie;
		this.serie = serie;
		this.creditos = creditos;
		this.idescuela = idescuela;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getSemestreinicio() {
		return semestreinicio;
	}

	public void setSemestreinicio(String semestreinicio) {
		this.semestreinicio = semestreinicio;
	}

	public String getSemestrefin() {
		return semestrefin;
	}

	public void setSemestrefin(String semestrefin) {
		this.semestrefin = semestrefin;
	}

	public Date getFechainicio() {
		return fechainicio;
	}

	public void setFechainicio(Date fechainicio) {
		this.fechainicio = fechainicio;
	}

	public Date getFechafin() {
		return fechafin;
	}

	public void setFechafin(Date fechafin) {
		this.fechafin = fechafin;
	}

	public Date getFechaserie() {
		return fechaserie;
	}

	public void setFechaserie(Date fechaserie) {
		this.fechaserie = fechaserie;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public Long getCreditos() {
		return creditos;
	}

	public void setCreditos(Long creditos) {
		this.creditos = creditos;
	}

	public Escuela getIdescuela() {
		return idescuela;
	}

	public void setIdescuela(Escuela idescuela) {
		this.idescuela = idescuela;
	}
}
