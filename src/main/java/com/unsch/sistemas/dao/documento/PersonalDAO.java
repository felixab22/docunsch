package com.unsch.sistemas.dao.documento;

import org.springframework.data.jpa.repository.JpaRepository;
import com.unsch.sistemas.model.documento.Personal;
import java.util.List;

public interface PersonalDAO extends JpaRepository<Personal, Long> {
    List<Personal> findByIdpersona(Long idpersonal);
}