package com.unsch.sistemas.dao.documento;

import org.springframework.data.jpa.repository.JpaRepository;
import com.unsch.sistemas.model.documento.Unico;
import java.util.List;

public interface UnicoDAO extends JpaRepository<Unico, Long> {
    List<Unico> findByIdunico(Long idunico);
}