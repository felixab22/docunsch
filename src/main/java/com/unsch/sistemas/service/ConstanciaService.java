package com.unsch.sistemas.service;

import java.util.List;
import com.unsch.sistemas.model.documento.Constancia;

public interface ConstanciaService {

    Constancia SaveConstancia(Constancia constancia);

    List<Constancia> GetListaConstancia();
}
