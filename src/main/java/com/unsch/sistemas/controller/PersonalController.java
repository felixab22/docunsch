package com.unsch.sistemas.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.unsch.sistemas.model.documento.Personal;
import com.unsch.sistemas.service.PersonalService;
import com.unsch.sistemas.util.RestResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/personal")
public class PersonalController {

    @Autowired
    protected PersonalService alumnoSrv;
    // convertir JSON en entitities y al reves
    @Autowired
    protected ObjectMapper objPersonal;

    @RequestMapping(value = "/SaveOrUpdatePersonal", method = RequestMethod.POST)
    public RestResponse GuardarPersonal(@RequestBody String jsonPersonal)
            throws JsonParseException, JsonMappingException, IOException {
        this.objPersonal = new ObjectMapper();

        Personal personal = this.objPersonal.readValue(jsonPersonal, Personal.class);

        // hacer validaciones

        // decimos para guardar
        this.alumnoSrv.SavePersonal(personal);
        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa", personal);
    }

    @GetMapping("/listaPersonals")
    public RestResponse GetListaPersonal() {
 List<Personal> result = this.alumnoSrv.GetListaPersonal();
 if (result.isEmpty()){
    return new RestResponse(HttpStatus.NO_CONTENT.value(),"No se encuentra registrado ningun tipo de documento");
}
return new RestResponse(HttpStatus.OK.value(),"Registros ubicados", result);
}
}
