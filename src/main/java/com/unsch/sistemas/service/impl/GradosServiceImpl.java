package com.unsch.sistemas.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unsch.sistemas.dao.documento.GradosDAO;
import com.unsch.sistemas.model.documento.Grados;
import com.unsch.sistemas.service.GradosService;
import java.util.List;

@Service
public class GradosServiceImpl implements GradosService {

    @Autowired
    protected GradosDAO daoGrados;

    @Override
    public List<Grados> GetListaGrados() {

        return this.daoGrados.findAll();
    }

    @Override
    public Grados SaveGrados(Grados grados) {

        return this.daoGrados.save(grados);
    }
}
