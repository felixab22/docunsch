package com.unsch.sistemas.service;

import com.unsch.sistemas.model.documento.User;

/**
 * Created by AITAMH on 3/06/2020.
 */

public interface IUserService {

    /**
     * Obtiene un usuario con su username
     * @param username
     * @return
     */
    User getPersonaByUsuario(String username);
}
