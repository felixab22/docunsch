package com.unsch.sistemas.model.documento;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "estadodocumento")
public class Estadodocumento {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idestadodocu")
    private Long idestadodocu;

    @Column(name = "observacion")
    private String observacion;

    @Column(name = "pie")
    private String pie;

    @Column(name = "fechatramite")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date fechatramite;

    @ManyToOne
    @JoinColumn(name = "idestado")
    private Estado idestado;

    @ManyToOne
    @JoinColumn(name = "idsolicitud")
    @JsonBackReference
    private Solicitud idsolicitud;

    public Estadodocumento() {
    }

    public Estadodocumento(Solicitud idsolicitud) {
        this.idsolicitud = idsolicitud;
    }

    public Estadodocumento(String observacion, String pie, Date fechatramite, Estado idestado, Solicitud idsolicitud) {
        this.observacion = observacion;
        this.pie = pie;
        this.fechatramite = fechatramite;
        this.idestado = idestado;
        this.idsolicitud = idsolicitud;
    }

    public Long getIdestadodocu() {
        return idestadodocu;
    }

    public void setIdestadodocu(Long idestadodocu) {
        this.idestadodocu = idestadodocu;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Date getFechatramite() {
        return fechatramite;
    }

    public void setFechatramite(Date fechatramite) {
        this.fechatramite = fechatramite;
    }

    public Estado getIdestado() {
        return idestado;
    }

    public void setIdestado(Estado idestado) {
        this.idestado = idestado;
    }

    public Solicitud getIdsolicitud() {
        return idsolicitud;
    }

    public void setIdsolicitud(Solicitud idsolicitud) {
        this.idsolicitud = idsolicitud;
    }

    public String getPie() {
        return pie;
    }

    public void setPie(String pie) {
        this.pie = pie;
    }

    @PrePersist
    public void prePersist(){
        fechatramite = new Date();
    }
}
