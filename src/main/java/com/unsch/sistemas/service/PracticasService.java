package com.unsch.sistemas.service;

import java.util.List;

import com.unsch.sistemas.model.documento.Practicas;

public interface PracticasService {

    Practicas SavePracticas(Practicas practicas);

    List<Practicas> GetListaPracticas();
}
