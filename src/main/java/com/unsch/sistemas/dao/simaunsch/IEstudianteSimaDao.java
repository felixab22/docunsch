package com.unsch.sistemas.dao.simaunsch;

import com.unsch.sistemas.model.simaunch.EstudianteSima;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IEstudianteSimaDao extends JpaRepository<EstudianteSima, Long> {

    EstudianteSima findByCodigo(String codigo);
}
