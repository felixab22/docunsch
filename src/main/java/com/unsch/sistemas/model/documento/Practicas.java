package com.unsch.sistemas.model.documento;

import javax.persistence.*;
import java.util.Date;
import java.io.Serializable;


@Entity
@Table(name = "practicas")
@Access(AccessType.FIELD)
public class Practicas implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "idpracticas")
	private Long idpracticas;

	@Column(name = "fechapracticas")
	@Temporal(TemporalType.DATE)
	private Date fechapracticas;

	@Column(name = "juradouno")
	private String juradouno;

	@Column(name = "juradodos")
	private String juradodos;

	@Column(name = "juradotres")
	private String juradotres;

	@Column(name = "notainforme")
	private Long notainforme;

	@Column(name = "notapreguntas")
	private Long notapreguntas;

	@Column(name = "notaexposicion")
	private Long notaexposicion;

	@Column(name = "semestre")
	private String semestre;

	@Column(name = "titulo")
	private String titulo;

	@Column(name = "lugar")
	private String lugar;

	@Column(name = "hora")
	private String hora;

	@JoinColumn(name = "idpersona", referencedColumnName = "idpersona")
	@ManyToOne
	private Estudiante idestudiante;

	public Practicas() {

	}

	public Practicas(Long idpracticas) {
		this.idpracticas = idpracticas;
	}

	public Practicas(Long idpracticas, Date fechapracticas, String juradouno, String juradodos, String juradotres,
					 Long notainforme, Long notapreguntas, Long notaexposicion, String semestre, String titulo,
			String lugar, String hora, Estudiante idestudiante) {

		this.idpracticas = idpracticas;
		this.fechapracticas = fechapracticas;
		this.juradouno = juradouno;
		this.juradodos = juradodos;
		this.juradotres = juradotres;
		this.notainforme = notainforme;
		this.notapreguntas = notapreguntas;
		this.notaexposicion = notaexposicion;
		this.semestre = semestre;
		this.titulo = titulo;
		this.lugar = lugar;
		this.hora = hora;
		this.idestudiante = idestudiante;
	}

	public Long getIdpracticas() {
		return idpracticas;
	}

	public void setIdpracticas(Long idpracticas) {
		this.idpracticas = idpracticas;
	}

	public Date getFechapracticas() {
		return fechapracticas;
	}

	public void setFechapracticas(Date fechapracticas) {
		this.fechapracticas = fechapracticas;
	}

	public String getJuradouno() {
		return juradouno;
	}

	public void setJuradouno(String juradouno) {
		this.juradouno = juradouno;
	}

	public String getJuradodos() {
		return juradodos;
	}

	public void setJuradodos(String juradodos) {
		this.juradodos = juradodos;
	}

	public String getJuradotres() {
		return juradotres;
	}

	public void setJuradotres(String juradotres) {
		this.juradotres = juradotres;
	}

	public Long getNotainforme() {
		return notainforme;
	}

	public void setNotainforme(Long notainforme) {
		this.notainforme = notainforme;
	}

	public Long getNotapreguntas() {
		return notapreguntas;
	}

	public void setNotapreguntas(Long notapreguntas) {
		this.notapreguntas = notapreguntas;
	}

	public Long getNotaexposicion() {
		return notaexposicion;
	}

	public void setNotaexposicion(Long notaexposicion) {
		this.notaexposicion = notaexposicion;
	}

	public String getSemestre() {
		return semestre;
	}

	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getLugar() {
		return lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public Estudiante getIdestudiante() {
		return idestudiante;
	}

	public void setIdestudiante(Estudiante idestudiante) {
		this.idestudiante = idestudiante;
	}

	@Override
	public String toString() {
		return "Practicas{" + "idpracticas=" + idpracticas + ", fechapracticas='" + fechapracticas + ", juradouno='"
				+ juradouno + ", juradodos='" + juradodos + ", juradotres='" + juradotres + ", notainforme='"
				+ notainforme + ", notapreguntas='" + notapreguntas + ", notaexposicion='" + notaexposicion
				+ ", semestre='" + semestre + ", titulo='" + titulo + ", lugar='" + lugar + ", hora='" + hora
				+ ", idestudiante='" + idestudiante + '}';
	}

}
