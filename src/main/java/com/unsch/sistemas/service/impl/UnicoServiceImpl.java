package com.unsch.sistemas.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unsch.sistemas.dao.documento.UnicoDAO;
import com.unsch.sistemas.model.documento.Unico;
import com.unsch.sistemas.service.UnicoService;
import java.util.List;

@Service
public class UnicoServiceImpl implements UnicoService {

    @Autowired
    protected UnicoDAO daoUnico;

    @Override
    public List<Unico> GetListaUnico() {

        return this.daoUnico.findAll();
    }

    @Override
    public Unico SaveUnico(Unico unico) {

        return this.daoUnico.save(unico);
    }
}
