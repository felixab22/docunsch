package com.unsch.sistemas.dao.documento;

import org.springframework.data.jpa.repository.JpaRepository;
import com.unsch.sistemas.model.documento.Escuela;
import java.util.List;

public interface EscuelaDAO extends JpaRepository<Escuela, Long> {
    List<Escuela> findByIdescuela(Long idescuela);
}
