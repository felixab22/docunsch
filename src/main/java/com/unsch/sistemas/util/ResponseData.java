package com.unsch.sistemas.util;

/**
 * Created by AITAMH on 13/12/2019.
 */
public class ResponseData {

    private Integer code;
    private String message;
    private Object dataDocumento;
    private Object dataMovimientos;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getDataDocumento() {
        return dataDocumento;
    }

    public void setDataDocumento(Object dataDocumento) {
        this.dataDocumento = dataDocumento;
    }

    public Object getDataMovimientos() {
        return dataMovimientos;
    }

    public void setDataMovimientos(Object dataMovimientos) {
        this.dataMovimientos = dataMovimientos;
    }
}
