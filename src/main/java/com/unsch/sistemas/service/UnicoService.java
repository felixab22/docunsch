package com.unsch.sistemas.service;

import java.util.List;

import com.unsch.sistemas.model.documento.Unico;

public interface UnicoService {

    Unico SaveUnico(Unico unico);

    List<Unico> GetListaUnico();
}
