package com.unsch.sistemas.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.unsch.sistemas.model.documento.Practicas;
import com.unsch.sistemas.service.PracticasService;
import com.unsch.sistemas.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/practica")
public class PracticasController {

    @Autowired
    protected PracticasService alumnoSrv;
    // convertir JSON en entitities y al reves
    @Autowired
    protected ObjectMapper objPracticas;

    @RequestMapping(value = "/SaveOrUpdatePracticas", method = RequestMethod.POST)
    public RestResponse GuardarPracticas(@RequestBody String jsonPracticas)
            throws JsonParseException, JsonMappingException, IOException {
        this.objPracticas = new ObjectMapper();

        Practicas practicas = this.objPracticas.readValue(jsonPracticas, Practicas.class);

        // hacer validaciones

        // decimos para guardar
        this.alumnoSrv.SavePracticas(practicas);
        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa", practicas);
    }

    @GetMapping("/listaPracticass")
    public RestResponse listaPracticas() {

    List<Practicas> result = this.alumnoSrv.GetListaPracticas();
    if (result.isEmpty()){
        return new RestResponse(HttpStatus.NO_CONTENT.value(),"No se encuentra registrado ningun tipo de documento");
    }
    return new RestResponse(HttpStatus.OK.value(),"Registros ubicados", result);
    }
}
