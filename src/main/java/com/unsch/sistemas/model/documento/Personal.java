package com.unsch.sistemas.model.documento;

import javax.persistence.*;

@Entity
@PrimaryKeyJoinColumn(referencedColumnName="idpersona")
public class Personal extends Persona{

    @Column(name = "cargo")
    private String cargo;

    @Column(name = "codigoempleo")
    private String codigoempleo;

    @ManyToOne
    @JoinColumn(name = "idescuela", referencedColumnName = "idescuela")
    private Escuela idescuela;

    public Personal(String cargo, String codigoempleo, Escuela idescuela) {
        this.cargo = cargo;
        this.codigoempleo = codigoempleo;
        this.idescuela = idescuela;
    }

    public Personal(Long idpersona, String cargo, String codigoempleo, Escuela idescuela) {
        super(idpersona);
        this.cargo = cargo;
        this.codigoempleo = codigoempleo;
        this.idescuela = idescuela;
    }

    public Personal(String nombre, String apellido, String dni, String cargo, String codigoempleo, Escuela idescuela) {
        super(nombre, apellido, dni);
        this.cargo = cargo;
        this.codigoempleo = codigoempleo;
        this.idescuela = idescuela;
    }

    public Personal() {
    }

    public Escuela getIdescuela() {
        return idescuela;
    }

    public void setIdescuela(Escuela idescuela) {
        this.idescuela = idescuela;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getCodigoempleo() {
        return codigoempleo;
    }

    public void setCodigoempleo(String codigoempleo) {
        this.codigoempleo = codigoempleo;
    }
}
