package com.unsch.sistemas.service.impl;

import com.unsch.sistemas.dao.documento.IPersonaDao;
import com.unsch.sistemas.dao.documento.IUserDao;
import com.unsch.sistemas.model.documento.User;
import com.unsch.sistemas.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by AITAMH on 3/06/2020.
 */
@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private IUserDao usuarioDao;

    @Autowired
    private IPersonaDao personaDao;

    @Override
    public User getPersonaByUsuario(String username) {
        System.out.println("ESTOY EN EL METODO SERVICE USER >>> ");
        User user = this.usuarioDao.findPersonaByUsername(username);

//        System.out.println("ID EXTRAIDO DEL USUARIO >>>> " + user.getPersona());

        return user;
    }
}
