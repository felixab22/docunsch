package com.unsch.sistemas.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.unsch.sistemas.model.documento.Unico;
import com.unsch.sistemas.service.UnicoService;
import com.unsch.sistemas.util.RestResponse;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/unico")
public class UnicoController {

    @Autowired
    protected UnicoService alumnoSrv;
    // convertir JSON en entitities y al reves
    @Autowired
    protected ObjectMapper objUnico;

    @RequestMapping(value = "/SaveOrUpdateUnico", method = RequestMethod.POST)
    public RestResponse GuardarUnico(@RequestBody String jsonUnico)
            throws JsonParseException, JsonMappingException, IOException {
        this.objUnico = new ObjectMapper();

        Unico unico = this.objUnico.readValue(jsonUnico, Unico.class);

        // hacer validaciones

        // decimos para guardar
        this.alumnoSrv.SaveUnico(unico);
        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa", unico);
    }

    @GetMapping("/listaUnicos")
    public RestResponse GetListaUnico() {
     List<Unico> result = this.alumnoSrv.GetListaUnico();
     if (result.isEmpty()){
        return new RestResponse(HttpStatus.NO_CONTENT.value(),"No se encuentra registrado ningun tipo de documento");
    }
    return new RestResponse(HttpStatus.OK.value(),"Registros ubicados", result);
    }
}
