package com.unsch.sistemas.service;

import java.util.List;

import com.unsch.sistemas.dto.SolicitudTipoDto;
import com.unsch.sistemas.model.documento.Escuela;
import com.unsch.sistemas.model.documento.Estado;
import com.unsch.sistemas.model.documento.Estadodocumento;

public interface EstadodocumentoService {

    Estadodocumento SaveEstadodocumento(Estadodocumento estadodocumento);

    /**
     * Obtiene la lista de las SOLICITUDES segun ESTADO(Pendiente, observados, archivados)
     * @param idestado
     * @return
     */
    List<SolicitudTipoDto> GetListaEstadodocumento(Estado idestado, Escuela idescuela);

}
