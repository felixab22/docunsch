package com.unsch.sistemas.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.unsch.sistemas.model.documento.Tipodocumento;
import com.unsch.sistemas.service.TipodocumentoService;
import com.unsch.sistemas.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/tipodoc")
public class TipodocumentoController {

    @Autowired
    protected TipodocumentoService tipodocumentoSrv;
    // convertir JSON en entitities y al reves
    @Autowired
    protected ObjectMapper objTipodocumento;

    @RequestMapping(value = "/SaveOrUpdateTipodocumento", method = RequestMethod.POST)
    public RestResponse  GuardarTipodocumento(@RequestBody String jsonTipodocumento)
            throws JsonParseException, JsonMappingException, IOException {
        this.objTipodocumento = new ObjectMapper();

        Tipodocumento tipodocumento = this.objTipodocumento.readValue(jsonTipodocumento, Tipodocumento.class);

        // hacer validaciones

        // decimos para guardar
        this.tipodocumentoSrv.SaveTipodocumento(tipodocumento);
        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa", tipodocumento);
    }

    @GetMapping("/listaTipodocumentos")
    public RestResponse GetListaTipodocumento(){
    List<Tipodocumento> result = this.tipodocumentoSrv.GetListaTipodocumento();
        if (result.isEmpty()){
            return new RestResponse(HttpStatus.NO_CONTENT.value(),"No se encuentra registrado ningun tipo de documento");
        }
        return new RestResponse(HttpStatus.OK.value(),"Registros ubicados", result);
    }
}
