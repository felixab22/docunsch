package com.unsch.sistemas.model.documento;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "carta")
@Access(AccessType.FIELD)
public class Carta implements Serializable {
	
	private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idcarta")
    private Long idcarta;
    
    @Column(name = "organizacion")
    private String organizacion;
    
    @Column(name = "nombrerepres")
    private String nombrerepres;
    
    @Column(name = "cargo")
    private String cargo;
    
    @Column(name = "area")
    private String area;

    @Column(name = "ciudad")
    private String ciudad;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "fechainicio")
    @Temporal(TemporalType.DATE)
    private Date fechainicio;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "fechafin")
    @Temporal(TemporalType.DATE)
    private Date fechafin;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "fechaemision")
    @Temporal(TemporalType.DATE)
    private Date fechaemision;
    
    @Column(name = "numcarta")
    private String numcarta;
    
    @JoinColumn(name = "idsolicitud", referencedColumnName = "idsolicitud")
    @ManyToOne
    private Solicitud idsolicitud;

	public Carta() {		
	}

	public Carta(Long idcarta) {		
		this.idcarta = idcarta;
	}

	public Carta(Long idcarta, String organizacion, String nombrerepres, String cargo, String area, String ciudad, Date fechainicio,
			Date fechafin, Date fechaemision, String numcarta, Solicitud idsolicitud) {
		this.idcarta = idcarta;
		this.organizacion = organizacion;
		this.nombrerepres = nombrerepres;
		this.cargo = cargo;
		this.area = area;
		this.ciudad = ciudad;
		this.fechainicio = fechainicio;
		this.fechafin = fechafin;
		this.fechaemision = fechaemision;
		this.numcarta = numcarta;
		this.idsolicitud = idsolicitud;
	}

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getIdcarta() {
        return idcarta;
    }

    public void setIdcarta(Long idcarta) {
        this.idcarta = idcarta;
    }

    public String getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(String organizacion) {
        this.organizacion = organizacion;
    }

    public String getNombrerepres() {
        return nombrerepres;
    }

    public void setNombrerepres(String nombrerepres) {
        this.nombrerepres = nombrerepres;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Date getFechainicio() {
        return fechainicio;
    }

    public void setFechainicio(Date fechainicio) {
        this.fechainicio = fechainicio;
    }

    public Date getFechafin() {
        return fechafin;
    }

    public void setFechafin(Date fechafin) {
        this.fechafin = fechafin;
    }

    public Date getFechaemision() {
        return fechaemision;
    }

    public void setFechaemision(Date fechaemision) {
        this.fechaemision = fechaemision;
    }

    public String getNumcarta() {
        return numcarta;
    }

    public void setNumcarta(String numcarta) {
        this.numcarta = numcarta;
    }

    public Solicitud getIdsolicitud() {
        return idsolicitud;
    }

    public void setIdsolicitud(Solicitud idsolicitud) {
        this.idsolicitud = idsolicitud;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    @Override
    public String toString() {
        return "grados{" +
                "idcarta=" + idcarta +
                ", organizacion='" + organizacion + 
                ", nombrerepres='" + nombrerepres + 
                ", cargo='" + cargo + 
                ", area='" + area +
                ", ciudad='" + ciudad +
                ", fechainicio='" + fechainicio + 
                ", fechafin='" + fechafin + 
                ", fechaemision='" + fechaemision + 
                ", numcarta='" + numcarta + 
                ", idsolicitud='" + idsolicitud + 
                '}';
    } 
	
    
}
