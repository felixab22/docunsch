package com.unsch.sistemas.dto;

import com.unsch.sistemas.model.documento.Estadodocumento;
import com.unsch.sistemas.model.documento.Estudiante;

import java.util.Date;
import java.util.List;

public class SolicitudEstadosDto {

    private Long idsolicitud;
    private String codigodocumento;
    private Date fechasolicitud;
    private String asunto;
    private String voucher;
    private Estudiante estudiante;
    private Long idtipodocumento;


    private List<Estadodocumento> estadodos;

    public Long getIdsolicitud() {
        return idsolicitud;
    }

    public void setIdsolicitud(Long idsolicitud) {
        this.idsolicitud = idsolicitud;
    }

    public String getCodigodocumento() {
        return codigodocumento;
    }

    public void setCodigodocumento(String codigodocumento) {
        this.codigodocumento = codigodocumento;
    }

    public Date getFechasolicitud() {
        return fechasolicitud;
    }

    public void setFechasolicitud(Date fechasolicitud) {
        this.fechasolicitud = fechasolicitud;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getVoucher() {
        return voucher;
    }

    public void setVoucher(String voucher) {
        this.voucher = voucher;
    }

    public Estudiante getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(Estudiante estudiante) {
        this.estudiante = estudiante;
    }

    public Long getIdtipodocumento() {
        return idtipodocumento;
    }

    public void setIdtipodocumento(Long idtipodocumento) {
        this.idtipodocumento = idtipodocumento;
    }

    public List<Estadodocumento> getEstadodos() {
        return estadodos;
    }

    public void setEstadodos(List<Estadodocumento> estadodos) {
        this.estadodos = estadodos;
    }
}
