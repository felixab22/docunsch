package com.unsch.sistemas.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "docEntityManagerFactory", transactionManagerRef = "docTransactionManager", basePackages = {
        "com.unsch.sistemas.dao.documento"})
public class docDbConfig {

    @Primary
    @Bean(name = "docDataSource")
    @ConfigurationProperties(prefix = "spring.documento.datasource")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }

    @Primary
    @Bean(name = "docEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean docEntityManagerFactory(EntityManagerFactoryBuilder builder,
                                                                          @Qualifier("docDataSource") DataSource dataSource) {
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", "update");
        properties.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
        return builder.dataSource(dataSource).properties(properties)
                .packages("com.unsch.sistemas.model.documento").build();
    }

    @Primary
    @Bean(name = "docTransactionManager")
    public PlatformTransactionManager docTransactionManager(
            @Qualifier("docEntityManagerFactory") EntityManagerFactory docEntityManagerFactory) {
        return new JpaTransactionManager(docEntityManagerFactory);
    }
}
