package com.unsch.sistemas.model.documento;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by AITAMH on 12/12/2019.
 */
@Entity
@Table(name = "estado")
public class Estado implements Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idestado")
    private Long idestado;

    @Column(name = "denominacion")
    private String denominacion;

    public Estado() {
    }

    public Estado(Long idestado) {
        this.idestado = idestado;
    }

    public Estado(String denominacion) {
        this.denominacion = denominacion;
    }

    public Long getIdestado() {
        return idestado;
    }

    public void setIdestado(Long idestado) {
        this.idestado = idestado;
    }

    public String getDenominacion() {
        return denominacion;
    }

    public void setDenominacion(String denominacion) {
        this.denominacion = denominacion;
    }
}
