package com.unsch.sistemas.dto;

import com.unsch.sistemas.model.documento.Estudiante;
import com.unsch.sistemas.model.documento.Tipodocumento;

import java.util.Date;

public class SolicitudTipoDto {

    private Long idestadodocumento;
    private Long idsolicitud;
    private String codigodocumento;
    private Date fechasolicitud;
    private String asunto;
    private String voucher;
    private Estudiante estudiante;
    private Long tipodocumento;

    public Long getIdestadodocumento() {
        return idestadodocumento;
    }

    public void setIdestadodocumento(Long idestadodocumento) {
        this.idestadodocumento = idestadodocumento;
    }

    public Long getIdsolicitud() {
        return idsolicitud;
    }

    public void setIdsolicitud(Long idsolicitud) {
        this.idsolicitud = idsolicitud;
    }

    public String getCodigodocumento() {
        return codigodocumento;
    }

    public void setCodigodocumento(String codigodocumento) {
        this.codigodocumento = codigodocumento;
    }

    public Date getFechasolicitud() {
        return fechasolicitud;
    }

    public void setFechasolicitud(Date fechasolicitud) {
        this.fechasolicitud = fechasolicitud;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getVoucher() {
        return voucher;
    }

    public void setVoucher(String voucher) {
        this.voucher = voucher;
    }

    public Estudiante getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(Estudiante estudiante) {
        this.estudiante = estudiante;
    }

    public Long getTipodocumento() {
        return tipodocumento;
    }

    public void setTipodocumento(Long tipodocumento) {
        this.tipodocumento = tipodocumento;
    }
}
