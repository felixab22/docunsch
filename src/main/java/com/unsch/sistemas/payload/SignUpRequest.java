package com.unsch.sistemas.payload;

import com.unsch.sistemas.model.documento.Personal;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * Created by HeverFernandez on 02/08/17.
 */

public class SignUpRequest {

    @Size(min = 3, max = 15)
    private String username;

    @NotBlank
    @Size(min = 6, max = 20)
    private String password;

    private Personal personal;

    private Long idrole;

    public Personal getPersonal() {
        return personal;
    }

    public void setPersonal(Personal personal) {
        this.personal = personal;
    }

    public void setIdrole(Long idrole) {
        this.idrole = idrole;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getIdrole() {
        return idrole;
    }

    public void setRole(Long idrole) {
        this.idrole = idrole;
    }


    @Override
    public String toString() {
        return "SignUpRequest{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +

                ", idrole=" + idrole +
                '}';
    }
}

