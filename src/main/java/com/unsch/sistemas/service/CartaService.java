package com.unsch.sistemas.service;

import java.util.List;
import com.unsch.sistemas.model.documento.Carta;
import com.unsch.sistemas.model.documento.Solicitud;

public interface CartaService {

	Carta SaveCarta(Carta carta);

	List<Carta> GetListaCarta();

	Carta buscarCartaByIdsolicitud(Solicitud idsolicitud);

}
