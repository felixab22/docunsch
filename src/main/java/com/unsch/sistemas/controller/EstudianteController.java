package com.unsch.sistemas.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.unsch.sistemas.model.documento.Estudiante;
import com.unsch.sistemas.service.EstudianteService;
import com.unsch.sistemas.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("estudiante")
public class EstudianteController {

    @Autowired
    protected EstudianteService EstudianteSrv;
    // convertir JSON en entitities y al reves
    @Autowired
    protected ObjectMapper objEstudiante;

    @RequestMapping(value = "/SaveOrUpdateEstudiante", method = RequestMethod.POST)
    public RestResponse GuardarEstudiante(@RequestBody String jsonEstudiante)
            throws JsonParseException, JsonMappingException, IOException {
        this.objEstudiante = new ObjectMapper();

        Estudiante estudiante = this.objEstudiante.readValue(jsonEstudiante, Estudiante.class);

        // hacer validaciones

        // decimos para guardar
        this.EstudianteSrv.SaveEstudiante(estudiante);
        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa", estudiante);
    }

    @GetMapping("/listaEstudiantes")
    public List<Estudiante> listaEstudiante() {
        return this.EstudianteSrv.GetListaEstudiante();
    }

    @GetMapping("/getEstudianteByCodigo")
    public RestResponse getEstudianteByCodigo(@RequestParam String codigo){

        Estudiante estudiante = this.EstudianteSrv.getEstudianteByCodigo(codigo);

        if (estudiante == null){
            return new RestResponse(HttpStatus.NO_CONTENT.value(),"Estudiante no se encuentra registrado");
        }

        return new RestResponse(HttpStatus.OK.value(),"Datos encontrados", estudiante);
    }
}
