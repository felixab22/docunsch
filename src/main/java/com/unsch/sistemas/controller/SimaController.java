package com.unsch.sistemas.controller;

import com.unsch.sistemas.dao.simaunsch.IEstudianteSimaDao;
import com.unsch.sistemas.model.simaunch.EstudianteSima;
import com.unsch.sistemas.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/sima")
public class SimaController {

    @Autowired
    private IEstudianteSimaDao estudianteSimaDao;

    @GetMapping("/getEstudianteByCodigo")
    public RestResponse getEstudianteByCodigo(@RequestParam String codigo){

        EstudianteSima result = this.estudianteSimaDao.findByCodigo(codigo);

        if (result == null){
            return new RestResponse(HttpStatus.NO_CONTENT.value(),"Registro no encontrado");
        }
        return new RestResponse(HttpStatus.OK.value(),"Registro encontrado",result);
    }
}
