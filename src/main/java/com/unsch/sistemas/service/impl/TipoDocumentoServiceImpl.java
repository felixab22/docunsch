package com.unsch.sistemas.service.impl;

import com.unsch.sistemas.dao.documento.TipodocumentoDAO;
import com.unsch.sistemas.model.documento.Tipodocumento;
import com.unsch.sistemas.service.TipodocumentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by AITAMH on 12/12/2019.
 */
@Service
public class TipoDocumentoServiceImpl implements TipodocumentoService {

    @Autowired
    private TipodocumentoDAO tipodocumentoDAO;

    @Override
    public Tipodocumento SaveTipodocumento(Tipodocumento tipodocumento) {
        return this.tipodocumentoDAO.save(tipodocumento);
    }

    @Override
    public List<Tipodocumento> GetListaTipodocumento() {
        return this.tipodocumentoDAO.findAll();
    }
}
