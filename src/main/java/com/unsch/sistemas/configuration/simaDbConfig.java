package com.unsch.sistemas.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "acadEntityManagerFactory", transactionManagerRef = "acadTransactionManager", basePackages = {
        "com.unsch.sistemas.dao.simaunsch" })
public class simaDbConfig {

    @Bean(name = "acadDataSource")
    @ConfigurationProperties(prefix = "spring.simaunsch.datasource")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "acadEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean acadEntityManagerFactory(EntityManagerFactoryBuilder builder,
                                                                          @Qualifier("acadDataSource") DataSource dataSource) {
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", "update");
        properties.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
        return builder.dataSource(dataSource).properties(properties)
                .packages("com.unsch.sistemas.model.simaunch").build();
    }

    @Bean(name = "acadTransactionManager")
    public PlatformTransactionManager acadTransactionManager(
            @Qualifier("acadEntityManagerFactory") EntityManagerFactory acadEntityManagerFactory) {
        return new JpaTransactionManager(acadEntityManagerFactory);
   }
}
