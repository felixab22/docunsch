package com.unsch.sistemas.service;

import java.util.List;

import com.unsch.sistemas.model.documento.Tipodocumento;

public interface TipodocumentoService {

    Tipodocumento SaveTipodocumento(Tipodocumento tipodocumento);

    List<Tipodocumento> GetListaTipodocumento();
}
