package com.unsch.sistemas.dao.documento;

import org.springframework.data.jpa.repository.JpaRepository;
import com.unsch.sistemas.model.documento.Tipodocumento;
import java.util.List;

public interface TipodocumentoDAO extends JpaRepository<Tipodocumento, Long> {
    List<Tipodocumento> findByIdtipodocumento(Long idtipodocumento);
}
