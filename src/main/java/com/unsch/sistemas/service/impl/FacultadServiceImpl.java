package com.unsch.sistemas.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unsch.sistemas.dao.documento.FacultadDAO;
import com.unsch.sistemas.model.documento.Facultad;
import com.unsch.sistemas.service.FacultadService;
import java.util.List;

@Service
public class FacultadServiceImpl implements FacultadService {

    @Autowired
    protected FacultadDAO daoFacultad;

    @Override
    public List<Facultad> GetListaFacultad() {

        return this.daoFacultad.findAll();
    }

    @Override
    public Facultad SaveFacultad(Facultad facultad) {

        return this.daoFacultad.save(facultad);
    }
}
