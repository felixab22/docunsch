package com.unsch.sistemas.dao.documento;

import com.unsch.sistemas.model.documento.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by AITAMH on 3/06/2020.
 */
public interface IRoleDao extends JpaRepository<Role, Long> {

    Role findByDescripcionrol(String roleName);

    @Query("SELECT r FROM Role r WHERE r.idrol = :idrol")
    Role findByIdrol(@Param("idrol") Long idrol);
}
