package com.unsch.sistemas.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.unsch.sistemas.model.documento.Carta;
import com.unsch.sistemas.model.documento.Solicitud;
import com.unsch.sistemas.service.CartaService;
import com.unsch.sistemas.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/carta")
public class CartaController {

    @Autowired
    protected CartaService alumnoSrv;
    // convertir JSON en entitities y al reves
    @Autowired
    protected ObjectMapper objCarta;

    @RequestMapping(value = "/SaveOrUpdateCarta", method = RequestMethod.POST)
    public RestResponse GuardarCarta(@RequestBody String jsonCarta)
            throws JsonParseException, JsonMappingException, IOException {
        this.objCarta = new ObjectMapper();

        Carta carta = this.objCarta.readValue(jsonCarta, Carta.class);

        // hacer validaciones

        // decimos para guardar
        this.alumnoSrv.SaveCarta(carta);
        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa", carta);       
    }

    @GetMapping("/listaCartas")
    public RestResponse GetListaCarta() {

     List<Carta> result = this.alumnoSrv.GetListaCarta();
     
    if (result.isEmpty()){
        return new RestResponse(HttpStatus.NO_CONTENT.value(),"No se encuentra registrado ningun tipo de documento");
    }
    return new RestResponse(HttpStatus.OK.value(),"Registros ubicados", result);
    }

    @GetMapping("/getCartaByIdsolicitud")
    public RestResponse GetBuscarCarta(@RequestParam Solicitud idsolicitud) {

      Carta cartaResul = this.alumnoSrv.buscarCartaByIdsolicitud(idsolicitud);

        if (cartaResul == null){
            return new RestResponse(HttpStatus.NO_CONTENT.value(),"No se encuentra la carta con el idsolicitud");
        }
        return new RestResponse(HttpStatus.OK.value(),"Registros ubicados", cartaResul);
    }
}
