package com.unsch.sistemas.model.documento;

import javax.persistence.*;


@Entity
@Table(name = "unico")
@Access(AccessType.FIELD)
public class Unico {

	private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idunico")
    private Long idunico;
    
    @Column(name = "siglauno")
    private String siglauno;
    
    @Column(name = "siglados")
    private String siglados;
    
    @Column(name = "siglatres")
    private String siglatres;
    
    @Column(name = "nombreuno")
    private String nombreuno;
    
    @Column(name = "nombredos")
    private String nombredos;
    
    @Column(name = "nombretres")
    private String nombretres;
    
    @Column(name = "creditouno")
    private String creditouno;
    
    @Column(name = "creditodos")
    private String creditodos;
    
    @Column(name = "creditotres")
    private String creditotres;
    
    @JoinColumn(name = "idsolicitud", referencedColumnName = "idsolicitud")
    @ManyToOne
    private Solicitud idsolicitud;

	public Unico() {
		
	}

	public Unico(Long idunico) {		
		this.idunico = idunico;
	}

	public Unico(Long idunico, String siglauno, String siglados, String siglatres, String nombreuno, String nombredos,
			String nombretres, String creditouno, String creditodos, String creditotres, Solicitud idsolicitud) {
		this.idunico = idunico;
		this.siglauno = siglauno;
		this.siglados = siglados;
		this.siglatres = siglatres;
		this.nombreuno = nombreuno;
		this.nombredos = nombredos;
		this.nombretres = nombretres;
		this.creditouno = creditouno;
		this.creditodos = creditodos;
		this.creditotres = creditotres;
		this.idsolicitud = idsolicitud;
	}

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getIdunico() {
        return idunico;
    }

    public void setIdunico(Long idunico) {
        this.idunico = idunico;
    }

    public String getSiglauno() {
        return siglauno;
    }

    public void setSiglauno(String siglauno) {
        this.siglauno = siglauno;
    }

    public String getSiglados() {
        return siglados;
    }

    public void setSiglados(String siglados) {
        this.siglados = siglados;
    }

    public String getSiglatres() {
        return siglatres;
    }

    public void setSiglatres(String siglatres) {
        this.siglatres = siglatres;
    }

    public String getNombreuno() {
        return nombreuno;
    }

    public void setNombreuno(String nombreuno) {
        this.nombreuno = nombreuno;
    }

    public String getNombredos() {
        return nombredos;
    }

    public void setNombredos(String nombredos) {
        this.nombredos = nombredos;
    }

    public String getNombretres() {
        return nombretres;
    }

    public void setNombretres(String nombretres) {
        this.nombretres = nombretres;
    }

    public String getCreditouno() {
        return creditouno;
    }

    public void setCreditouno(String creditouno) {
        this.creditouno = creditouno;
    }

    public String getCreditodos() {
        return creditodos;
    }

    public void setCreditodos(String creditodos) {
        this.creditodos = creditodos;
    }

    public String getCreditotres() {
        return creditotres;
    }

    public void setCreditotres(String creditotres) {
        this.creditotres = creditotres;
    }

    public Solicitud getIdsolicitud() {
        return idsolicitud;
    }

    public void setIdsolicitud(Solicitud idsolicitud) {
        this.idsolicitud = idsolicitud;
    }

    @Override
    public String toString() {
        return "Unico{"
                + "idunico=" + idunico
                + ", siglauno='" + siglauno
                + ", siglados='" + siglados
                + ", siglatres='" + siglatres
                + "nombreuno=" + nombreuno
                + "nombredos=" + nombredos
                + "nombretres=" + nombretres
                + "creditouno=" + creditouno
                + "creditodos=" + creditodos
                + "creditotres=" + creditotres
                + "idsolicitud=" + idsolicitud
                + '}';
    }

	
    
}
