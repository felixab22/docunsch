package com.unsch.sistemas.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.unsch.sistemas.model.documento.Constancia;
import com.unsch.sistemas.service.ConstanciaService;
import com.unsch.sistemas.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/constancia")
public class ConstanciaController {

    @Autowired
    protected ConstanciaService alumnoSrv;
    // convertir JSON en entitities y al reves
    @Autowired
    protected ObjectMapper objConstancia;

    @RequestMapping(value = "/SaveOrUpdateConstancia", method = RequestMethod.POST)
    public RestResponse GuardarConstancia(@RequestBody String jsonConstancia)
            throws JsonParseException, JsonMappingException, IOException {
        this.objConstancia = new ObjectMapper();

        Constancia constancia = this.objConstancia.readValue(jsonConstancia, Constancia.class);

        // hacer validaciones

        // decimos para guardar
        this.alumnoSrv.SaveConstancia(constancia);
        return new RestResponse(HttpStatus.OK.value(), "Operacion exitosa", constancia);
    }

    @GetMapping("/listaConstancias")
    public RestResponse listaConstancia() {
        List<Constancia> constancias = this.alumnoSrv.GetListaConstancia();

        if (constancias.isEmpty()){
            return new RestResponse(HttpStatus.NO_CONTENT.value(),"Lista vacia");
        }
        return new RestResponse(HttpStatus.OK.value(),"Registros encontrados",constancias);
    }

}
