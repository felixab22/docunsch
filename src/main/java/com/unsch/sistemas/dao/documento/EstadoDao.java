package com.unsch.sistemas.dao.documento;

import com.unsch.sistemas.model.documento.Estado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Created by AITAMH on 13/12/2019.
 */
public interface EstadoDao extends JpaRepository<Estado, Long> {

    @Query("SELECT e FROM Estado e WHERE e.idestado = 1")
    Estado getEstadoPendiente();
}
