package com.unsch.sistemas.service.impl;

import com.unsch.sistemas.dto.SolicitudTipoDto;
import com.unsch.sistemas.model.documento.Escuela;
import com.unsch.sistemas.model.documento.Estado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unsch.sistemas.dao.documento.EstadodocumentoDAO;
import com.unsch.sistemas.model.documento.Estadodocumento;
import com.unsch.sistemas.service.EstadodocumentoService;

import java.util.ArrayList;
import java.util.List;

@Service
public class EstadodocumentoServiceImpl implements EstadodocumentoService {

	@Autowired
	private EstadodocumentoDAO daoEstadodocumento;

	@Override
	public List<SolicitudTipoDto> GetListaEstadodocumento(Estado idestado, Escuela idescuela) {

		List<Estadodocumento> result = this.daoEstadodocumento.findAllByEstadoDocumento(idestado,idescuela);

		List<SolicitudTipoDto> resultDtos = new ArrayList<>();

		for (Estadodocumento estadodocumento: result){

			SolicitudTipoDto solicitudTipoDto = new SolicitudTipoDto();

			solicitudTipoDto.setIdestadodocumento(estadodocumento.getIdestadodocu());
			solicitudTipoDto.setIdsolicitud(estadodocumento.getIdsolicitud().getIdsolicitud());
			solicitudTipoDto.setCodigodocumento(estadodocumento.getIdsolicitud().getCodigo());
			solicitudTipoDto.setFechasolicitud(estadodocumento.getFechatramite());
			solicitudTipoDto.setAsunto(estadodocumento.getIdsolicitud().getTipodocumento().getNombre());
			solicitudTipoDto.setVoucher(estadodocumento.getIdsolicitud().getBoletaelectronica());
			solicitudTipoDto.setEstudiante(estadodocumento.getIdsolicitud().getIdestudiante());
			solicitudTipoDto.setTipodocumento(estadodocumento.getIdsolicitud().getTipodocumento().getIdtipodocumento());

			resultDtos.add(solicitudTipoDto);
		}

		return resultDtos;
	}

	@Override
	public Estadodocumento SaveEstadodocumento(Estadodocumento estadodocumento) {

		return this.daoEstadodocumento.save(estadodocumento);
	}
}
