package com.unsch.sistemas.dao.documento;

import org.springframework.data.jpa.repository.JpaRepository;
import com.unsch.sistemas.model.documento.Facultad;
import java.util.List;

public interface FacultadDAO extends JpaRepository<Facultad, Long> {
    List<Facultad> findByIdfacultad(Long idfacultad);
}