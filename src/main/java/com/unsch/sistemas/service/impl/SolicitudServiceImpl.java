package com.unsch.sistemas.service.impl;

import com.unsch.sistemas.dao.documento.EstadoDao;
import com.unsch.sistemas.dao.documento.EstadodocumentoDAO;
import com.unsch.sistemas.dto.SolicitudEstadosDto;
import com.unsch.sistemas.dto.SolicitudTipoDto;
import com.unsch.sistemas.model.documento.Estado;
import com.unsch.sistemas.model.documento.Estadodocumento;
import com.unsch.sistemas.model.documento.Tipodocumento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unsch.sistemas.dao.documento.SolicitudDAO;
import com.unsch.sistemas.model.documento.Solicitud;
import com.unsch.sistemas.service.SolicitudService;

import java.util.ArrayList;
import java.util.List;

@Service
public class SolicitudServiceImpl implements SolicitudService {

    @Autowired
    private SolicitudDAO daoSolicitud;

    @Autowired
    private EstadodocumentoDAO estadodocumentoDAO;

    @Autowired
    private EstadoDao estadoDao;

    @Override
    public List<Solicitud> GetListaSolicitud() {

        return this.daoSolicitud.findAll();
    }

    @Override
    public List<SolicitudTipoDto> getSolicitudByTipoDoc(Tipodocumento tipodocumento) {

        List<Solicitud> result = this.daoSolicitud.findAllByTipodocumento(tipodocumento);

        List<SolicitudTipoDto> resultDtos = new ArrayList<>();

        for (Solicitud solicitud: result){

            SolicitudTipoDto solicitudTipoDto = new SolicitudTipoDto();

            solicitudTipoDto.setIdsolicitud(solicitud.getIdsolicitud());
            solicitudTipoDto.setCodigodocumento(solicitud.getCodigo());
            solicitudTipoDto.setFechasolicitud(solicitud.getFechasolicitud());
            solicitudTipoDto.setAsunto(solicitud.getTipodocumento().getNombre());
            solicitudTipoDto.setVoucher(solicitud.getBoletaelectronica());
            solicitudTipoDto.setEstudiante(solicitud.getIdestudiante());
            solicitudTipoDto.setTipodocumento(solicitud.getTipodocumento().getIdtipodocumento());

            resultDtos.add(solicitudTipoDto);
        }

        return resultDtos;
    }

    @Override
    public SolicitudEstadosDto getSolicitudByCodigo(String codigo) {

        Solicitud solicitud = this.daoSolicitud.findByCodigo(codigo);

        if(solicitud == null){
            return null;
        }
        SolicitudEstadosDto estadosDto = new SolicitudEstadosDto();

        estadosDto.setIdsolicitud(solicitud.getIdsolicitud());
        estadosDto.setCodigodocumento(solicitud.getCodigo());
        estadosDto.setFechasolicitud(solicitud.getFechasolicitud());
        estadosDto.setAsunto(solicitud.getTipodocumento().getNombre());
        estadosDto.setVoucher(solicitud.getBoletaelectronica());
        estadosDto.setEstadodos(solicitud.getEstados());
        estadosDto.setEstudiante(solicitud.getIdestudiante());
        estadosDto.setIdtipodocumento(solicitud.getTipodocumento().getIdtipodocumento());

        return estadosDto;
    }

    @Override
    public Solicitud SaveSolicitud(Solicitud solicitud) {

        Solicitud solicitudData = daoSolicitud.findTopByOrderByIdsolicitudDesc();

        String codformat;

        if (solicitudData != null){
            codformat = String.format("%04d", (solicitudData.getIdsolicitud() + 1));

        }else {
            codformat = String.format("%04d",1);
        }
        solicitud.setCodigo("IS" + codformat);

        String codigoEstado="IS" + codformat;
        System.out.println("ESTE ES EL NUEVO CODIGO GENERADOO " + codigoEstado);

        Solicitud solicitudGuardada = this.daoSolicitud.save(solicitud);

        Estado estadoDoc = this.estadoDao.getEstadoPendiente();
        System.out.println("TABLA ESTADO ACCEDIDO >>>>>>>>>>>>>>>>>>>>>");
        Boolean existSocilitud = this.estadodocumentoDAO.existsByIdsolicitud(solicitudGuardada);

        System.out.println("SUN ID DELA SOLLICITUD ES " + solicitudGuardada.getIdsolicitud());

        if (!existSocilitud){
            System.out.println("ESTOY EN EL IF DE ESTADOO  >>>>>>>>");
            Estadodocumento estado = new Estadodocumento();
            estado.setObservacion("ENVIADO");
            estado.setIdestado(estadoDoc);
            estado.setIdsolicitud(solicitudGuardada);
            this.estadodocumentoDAO.save(estado);
            System.out.println("SE GUARDO CORRECTAMENTE EL ESTADO MOVIMIENTO ");
        }

        return solicitudGuardada;
    }
}

