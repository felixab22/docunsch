package com.unsch.sistemas.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unsch.sistemas.dao.documento.EstudianteDAO;
import com.unsch.sistemas.model.documento.Estudiante;
import com.unsch.sistemas.service.EstudianteService;
import java.util.List;

@Service
public class EstudianteServiceImpl implements EstudianteService {

	@Autowired
	protected EstudianteDAO daoEstudiante;

	@Override
	public List<Estudiante> GetListaEstudiante() {

		return this.daoEstudiante.findAll();
	}

	@Override
	public Estudiante getEstudianteByCodigo(String codigo) {
		return this.daoEstudiante.findEstudianteByCodigo(codigo);
	}

	@Override
	public Estudiante SaveEstudiante(Estudiante estudiante) {

		return this.daoEstudiante.save(estudiante);
	}
}
