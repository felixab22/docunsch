package com.unsch.sistemas.dao.documento;

import org.springframework.data.jpa.repository.JpaRepository;
import com.unsch.sistemas.model.documento.Grados;

import java.util.List;

public interface GradosDAO extends JpaRepository<Grados, Long> {
    List<Grados> findByIdgrados(Long idgrados);
}